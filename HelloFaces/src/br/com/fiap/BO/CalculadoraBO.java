package br.com.fiap.BO;

import java.text.NumberFormat;

public class CalculadoraBO {
	public static String formatedCalc(Double n1, Double n2, String op){

		Double resultado = null;

		switch (op) {
		case "+":
			resultado = n1 + n2;
			break;

		case "-":
			resultado = n1 - n2;
			break;
		case "*":
			resultado = n1*n2;
			break;
		case "/":
			if(n2 == 0 ) throw new ArithmeticException("N�o h� divis�o po 0");
			resultado = n1/n2;
			break;

		}
		
		
		NumberFormat format = NumberFormat.getInstance();
		format.setMinimumFractionDigits(0);
		format.setMaximumFractionDigits(2);

		return format.format(resultado);
	}
}
