package br.com.fiap.managedBean;

import javax.faces.bean.ManagedBean;

import br.com.fiap.BO.CalculadoraBO;

@ManagedBean
public class HelloBean {
	private Double n1;
	private Double n2;
	private String op;

	//propriedades de sa�da (placeholders)
	private String resultado;
	private String log;


	public Double getN1() {
		return n1;
	}

	public void setN1(Double n1) {
		this.n1 = n1;
	}
	public Double getN2() {
		return n2;
	}
	public void setN2(Double n2) {
		this.n2 = n2;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}

	public String getResultado() {
		return resultado;
	}

	public String getLog() {
		return log;
	}

	//metodo action controller
	public String calcular(){
		try{
			resultado = CalculadoraBO.formatedCalc(n1, n2, op);
			
			return "sucesso";
		}catch (ArithmeticException e) {
			// TODO: handle exception
			log = e.getMessage();
			
			return "erro";
		}
	}

}
